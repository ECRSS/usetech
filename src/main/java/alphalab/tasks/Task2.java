package alphalab.tasks;


import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class Task2 {

    /**
     * Функция создает файл с числами
     *
     * @param filename имя файла
     */
    public static void createFile(String filename) throws IOException {
        try (FileWriter writer = new FileWriter(filename, false)) {
            List<Integer> arrayList = new ArrayList<>();
            for (int i = 0; i < 21; i++) {
                arrayList.add(i);
            }
            Collections.shuffle(arrayList);
            String fileString = new String(arrayList.toString());
            fileString = fileString.replace("[", "");
            fileString = fileString.replace("]", "");
            fileString = fileString.replaceAll("\\s+", "");
            writer.write(fileString);
        }
    }

    /**
     * Функция читает файл с числами
     *
     * @param filename имя файла
     * @return содержимое файла
     */
    public static String readFile(String filename) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        try (FileReader reader = new FileReader(filename)) {
            int c;
            while ((c = reader.read()) != -1) {
                stringBuilder.append((char) c);
            }
        }
        return stringBuilder.toString();
    }

    /**
     * Функция парсит строку с числами
     *
     * @param fileString строка с числами
     * @param delimiter разделитель
     * @return список с числами
     */
    public static List<Integer> parseString(String fileString, String delimiter) {
        String[] numbersStrings = fileString.split(delimiter);
        List<Integer> numbers = new ArrayList<>();
        for (String string : numbersStrings) {
            numbers.add(Integer.parseInt(string));
        }
        return numbers;
    }

    /**
     * Функция сортирует список с числами
     *
     * @param numbers список с числами
     * @return сортированный список
     */
    public static List<Integer> sort(List<Integer> numbers) {
        Collections.sort(numbers);
        return numbers;
    }

    /**
     * Функция переворачивает список с числами
     *
     * @param numbers список с числами
     * @return перевернутый список
     */
    public static List<Integer> reverse(List<Integer> numbers) {
        Collections.reverse(numbers);
        return numbers;
    }
}
