package alphalab.tasks;

public class Task3 {

    /**
     * Функция возвращает факториал числа
     *
     * @param n число
     * @return факториал
     */
    public static int calculateFactorial(int n) {
        if (n == 0) {
            return 1;
        }
        return n * calculateFactorial(n - 1);
    }
}
