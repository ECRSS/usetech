package alphalab;

import alphalab.tasks.Task2;
import alphalab.tasks.Task3;

import java.io.IOException;
import java.util.List;

public class Main {

    private static final String FILE_NAME = "file.txt";
    private static final String DELIMITER = ",";

    public static void main(String[] args) throws IOException {
        System.out.println("Task factorial");
        System.out.println(Task3.calculateFactorial(9));
        System.out.println(Task3.calculateFactorial(0));
        System.out.println(Task3.calculateFactorial(5));

        System.out.println("Task read File");
        Task2.createFile(FILE_NAME);
        String text = Task2.readFile(FILE_NAME);
        List<Integer> numbers = Task2.parseString(text, DELIMITER);
        System.out.println("Парсинг: " + numbers.toString());
        Task2.sort(numbers);
        System.out.println("Отсортировано: " + numbers.toString());
        Task2.reverse(numbers);
        System.out.println("По убыванию: " + numbers.toString());
    }
}
