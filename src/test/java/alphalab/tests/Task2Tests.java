package alphalab.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import alphalab.tasks.Task2;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task2Tests {

    private static final String TEST_FILE_NAME = "test_file.txt";
    private static final String REGEX_NUM_PATTERN = "[0-9]+";
    private static final String TEST_STRING = "21,20,19,18,17,16";
    private static final Integer[] UNSORTED_TEST_NUMBERS = {20, 21, 19, 17, 18, 16};
    private static final Integer[] REVERSE_TEST_NUMBERS = {21, 20, 19, 18, 17, 16};
    private static final Integer[] UNREVERSE_TEST_NUMBERS = {16, 17, 18, 19, 20, 21};

    @Test
    public void createFileTest() throws IOException {
        Task2.createFile(TEST_FILE_NAME);
        File testfile = new File(TEST_FILE_NAME);
        Assert.assertEquals(testfile.exists(), true);
        StringBuilder stringBuilder = new StringBuilder();
        try (FileReader reader = new FileReader(testfile)) {
            int c;
            while ((c = reader.read()) != -1) {
                stringBuilder.append((char) c);
            }
        }
        testfile.delete();
        Assert.assertEquals(testfile.exists(), false);
        int count = 0;
        String file = stringBuilder.toString();
        Pattern p = Pattern.compile(REGEX_NUM_PATTERN);
        Matcher m = p.matcher(file);
        while (m.find()) {
            count++;
        }
        Assert.assertEquals(count, 21);
    }

    @Test
    public void readFileTest() throws IOException {
        try (FileWriter writer = new FileWriter(TEST_FILE_NAME)) {
            writer.write(TEST_STRING);
        }
        Assert.assertEquals(Task2.readFile(TEST_FILE_NAME), TEST_STRING);
    }

    @Test
    public void parseStringTest() {
        Assert.assertEquals(Task2.parseString(TEST_STRING, ",").toArray(), REVERSE_TEST_NUMBERS);
    }


    @Test
    public void reverseTest() {
        Assert.assertEquals(Task2.reverse(Arrays.asList(UNREVERSE_TEST_NUMBERS)).toArray(), REVERSE_TEST_NUMBERS);
    }

    @Test
    public void sortingTest() {
        ArrayList<Integer> arrayList = new ArrayList<Integer>(Arrays.asList(UNSORTED_TEST_NUMBERS));
        Assert.assertEquals(Task2.sort(Arrays.asList(UNSORTED_TEST_NUMBERS)).toArray(), UNREVERSE_TEST_NUMBERS);
    }
}
