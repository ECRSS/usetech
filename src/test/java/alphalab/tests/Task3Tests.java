package alphalab.tests;

import alphalab.tasks.Task3;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Task3Tests {

    @Test
    public void factorialTest()
    {
        Assert.assertEquals(Task3.calculateFactorial(5),120);
    }
}
